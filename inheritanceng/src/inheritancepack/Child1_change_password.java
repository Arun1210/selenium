package inheritancepack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;

public class Child1_change_password extends Child_shop_edit {
    @BeforeTest
  public void beforeTest() {
  }
    @Test (priority=11)
    public void clickonchangepass() throws InterruptedException
   {
    Thread.sleep(5000);
   	driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div/div/div[1]/div[5]/div/div/div[3]/div/div/div/div[1]/ul/li[4]/a")).click();
   }
    @Test (priority=12)
    public void Allvalueblank() throws InterruptedException {
  	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
  	  Thread.sleep(1000);
  	  String autual_message = driver.findElement(By.xpath("//*[@id=\"passwordBlank\"]/span")).getText();
  	  String expected_message = "Current password cannot be empty";
  	  Assert.assertEquals(autual_message, expected_message);
    }
    @Test (priority=13)
    public void NewConfomvalueblank() throws InterruptedException
    {
  	  driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("arunsoni#1210");
  	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
  	  Thread.sleep(1000);
  	  String autual_message = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]/span")).getText();
  	  String expected_message = "New password cannot be empty";
  	  Assert.assertEquals(autual_message, expected_message);
    }
    @Test (priority=14)
    public void Newpassshort() throws InterruptedException
    {
  	  driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("22d");
  	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
  	  Thread.sleep(1000);
  	  String autual_message = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
  	  String expected_message = "Password must be 6 characters or more";
  	  Assert.assertEquals(autual_message, expected_message);
    }
    @Test (priority=15)
    public void Newpassonlychar() throws InterruptedException
    {
  	  driver.findElement(By.xpath("//*[@id=\"password1\"]")).clear();
  	  driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("arunsoni");
  	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
  	  Thread.sleep(1000);
  	  String autual_message = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
  	  String expected_message = "Password must be alphanumeric";
  	  Assert.assertEquals(autual_message, expected_message);
  	  
    }
    @Test (priority=16)
    public void confpassblank() throws InterruptedException
    {
  	  driver.findElement(By.xpath("//*[@id=\"password1\"]")).clear();
  	  driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("arunsoni#1210");
  	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
  	  
  	  Thread.sleep(1000);
  	  String autual_message = driver.findElement(By.xpath("//*[@id=\"password2Blank\"]/span")).getText();
  	  String expected_message = "Confirm password cannot be empty";
  	  Assert.assertEquals(autual_message, expected_message);
  	  
    }
    @Test (priority=17)
    public void confpassinvalid() throws InterruptedException
    {
  	  driver.findElement(By.xpath("//*[@id=\"password2\"]")).sendKeys("arunsoni12#");
  	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
  	  Thread.sleep(1000);
  	  String autual_message = driver.findElement(By.xpath("//*[@id=\"newEqualToConfirm\"]")).getText();
  	  String expected_message = "New password and confirm new password do not match";
  	  Assert.assertEquals(autual_message, expected_message);
    }
  @AfterTest
  public void afterTest() {
	  
  }

  }

