package inheritancepack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Child_shop_edit extends Parent {
  
  @BeforeTest
  public void beforeTest() {
  }
@Test (priority=4)
 public void clickonmyprofile() throws InterruptedException
{
	driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a")).click();
	Thread.sleep(2000);
	driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[4]")).click();
}
@Test (priority=5)
public void allvalueblank() throws InterruptedException
{
	driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
	driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
	driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();
	driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	
	  Thread.sleep(1000);
	  Thread.sleep(1000);
	  String autual_message = driver.findElement(By.xpath("//*[@id=\"checkBlank1\"]/span")).getText();
	  String expected_message = "First name cannot be blank";
	  Assert.assertEquals(autual_message, expected_message);
	
}
@Test (priority=6)
public void lastnamemobilnovalueblank() throws InterruptedException
{
	driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("Arun");
	driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	Thread.sleep(1000);
	  String autual_message = driver.findElement(By.xpath("//*[@id=\"checkBlank2\"]/span")).getText();
	  String expected_message = "Last name cannot be blank";
	  Assert.assertEquals(autual_message, expected_message);
}
@Test (priority=7)
public void mobilnovalueblank() throws InterruptedException
{
	driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("Soni");
	driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	Thread.sleep(1000);
	  String autual_message = driver.findElement(By.xpath("//*[@id=\"checkBlank3\"]/span")).getText();
	  String expected_message = "Mobile number cannot be blank";
	  Assert.assertEquals(autual_message, expected_message);
}
@Test (priority=8)
public void mobileinchar() throws InterruptedException {
   Thread.sleep(1000);
   driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("afsgfs");
   driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
   Thread.sleep(1000);
   String autual_message = driver.findElement(By.xpath("//*[@id=\"mnumber\"]/span")).getText();
   String expected_message = "Enter correct mobile number";
   Assert.assertEquals(autual_message, expected_message);

}
@Test (priority=9)
public void mobileinvalid() throws InterruptedException
{
   Thread.sleep(1000);
   driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();
   driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("814190836");
   driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
}
@Test (priority=10)
public void valid() throws InterruptedException
{
Thread.sleep(1000);
   driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();
   driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("8141908366");
   driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
   Thread.sleep(2000);
   String actual_message1 = driver.findElement(By.xpath("//*[@id=\"notification_85214fdf7d646b0d64eeb251383996d0\"]/div/div[2]")).getText();
   String expect_message1 = "Profile has been updated successfully.";
   Assert.assertEquals(actual_message1, expect_message1);
}
  @AfterTest
  public void afterTest() {
  }

}
