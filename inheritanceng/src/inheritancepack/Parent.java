package inheritancepack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;

public class Parent {
	public WebDriver driver;
  @BeforeClass
  /*public void beforeClass() throws InterruptedException {
	  System.setProperty("webdriver.gecko.driver", "D:\\Eclipse\\geckodriver-v0.26.0-win64//geckodriver.exe");
	  driver = new FirefoxDriver();
	    driver.get("https://www.shopclues.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Actions over = new Actions(driver);
		WebElement over1 = driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a"));
		over.moveToElement(over1).perform();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"show_loginpop\"]/span")).click();
  }*/
  @Parameters("browser")
	public void setup(String browser) throws Exception{
		if(browser.equalsIgnoreCase("firefox")){
			System.setProperty("webdriver.gecko.driver", "D:\\Eclipse\\geckodriver-v0.26.0-win64//geckodriver.exe");
			driver = new FirefoxDriver();
		}
		
		else if(browser.equalsIgnoreCase("chrome")){
			
			System.setProperty("webdriver.chrome.driver","D:\\Eclipse\\chromedriver_win32\\chromedriver.exe");
		
			driver = new ChromeDriver();
		}
  }
  
  @Test (priority=0)
  public void bothvalueblank() throws InterruptedException {
	  driver.get("https://www.shopclues.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Actions over = new Actions(driver);
		WebElement over1 = driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a"));
		over.moveToElement(over1).perform();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"show_loginpop\"]/span")).click();
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  Thread.sleep(2000);
	  String actual_message = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[2]/div[1]/span")).getText();
	  String expect_message = "Please enter email id or mobile number.";
	  Assert.assertEquals(actual_message, expect_message);
	  
	  String actual_message1 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
	  String expect_message1 = "Please enter your password.";
	  Assert.assertEquals(actual_message1, expect_message1);
	  
  }
  @Test (priority=1)
  public void shortpass() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("arunsoni1210@gmail.com");
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("123d");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  
	  Thread.sleep(2000);
	    
	  String actual_message1 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
	  String expect_message1 = "Password must be 6 characters or more.";
	  Assert.assertEquals(actual_message1, expect_message1);
  }
  @Test (priority=2)
  public void passwrong() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).clear();
	  Thread.sleep(1000);
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("arunsoni@1210");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  
	  Thread.sleep(2000);
	    
	  String actual_message1 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
	  String expect_message1 = "Incorrect login details.";
	  Assert.assertEquals(actual_message1, expect_message1);
  }
  
  @Test (priority=3)
  public void positivetestcase() throws InterruptedException
  {
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).clear();
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("arunsoni#1210");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("/html/body/div[3]/div[1]/div[5]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();
  }

  @AfterClass
  public void afterClass() {
  }
/*@AfterMethod
public void ss(ITestResult result)
{
	if(ITestResult.FAILURE==result.getStatus())
	{
		try
		{
			File obj = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(obj,new File("D:\\"+result.getName()+".png"));
			System.out.println("SS take successfully");
		}
		catch(Exception e)
		{
			System.out.println("Expection while takeing SS"+e.getMessage());
		}
		
	}
}*/
}
